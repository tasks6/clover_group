import random


def get_random_element_list(list_size: int, max_value: int) -> list:
    """ Метод, возвращающий список с случайными числами

    :param list_size: количество элементов (n)
    :param max_value: максимально возможное значение элемента
    """

    return [random.randint(1, max_value) for _ in range(list_size)]


def get_unique_element_list(elements_list: list, select_size: int) -> list:
    """ Метод, возвращающий новый список с неповторяющимися элементами

    :param elements_list: список элементов
    :param select_size: k элементов без повторений
    """

    selected_list = elements_list[:select_size]
    return list(set(selected_list))


if __name__ == '__main__':
    n = 100
    max_value = 60
    k = random.randint(1, n)

    random_list = get_random_element_list(n, max_value)
    response = get_unique_element_list(random_list, k)

    # print('Рандомные значения: ', random_list)
    # print('\nОтвет: ', response)
