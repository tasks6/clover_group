from datetime import datetime
import random


def get_new_datetime_dict(start_time: int, dict_size: int) -> dict:
    """ Создает и возвращает словарь {datetime: value}
    Время создается с интервалом 1 секунда, а значение - целое рандомное (1,100)

    :param start_time: начальная дата и время
    :param dict_size: количество значений в словаре (количество секунд)
    """

    datetime_dict = {}
    for second in range(dict_size):
        datetime_dict[f'{datetime.fromtimestamp(start_time+second)}'] = random.randint(1, 100)
    return datetime_dict


def get_100second_interval_dict(times_dict: dict) -> dict:
    """ Данный метод вычисляет среднее арифметическое, минимум,
    максимум, медиану для значений в интервале времени 100 секунд
    Возвращает словарь {'2016-11-04 14:56:50':
    {'average': 47.31, 'min': 1, 'max': 100, 'median': 47.5}, ...}

    :param times_dict: словарь в формате {'2016-11-04 14:56:50': 11, ...}
    """

    # Приводим словарь к списку
    times_list = [(key, value) for key, value in times_dict.items()]
    times_length = len(times_list)
    result = {}

    # Проверяем на наличие данных в списке
    if times_length < 1:
        return {}

    # Назначаем интервал
    index = 100
    if times_length < index:
        index = times_length

    # Проходим по списку каждые 100 секунд
    for _ in range(0, times_length, 100):
        data = {}
        # Берем интервал 100 секунд
        interval_values = times_list[_:index]

        # Берем значения из списка, сортируем их
        values = [element[1] for element in interval_values]
        values.sort()

        # Добавляем в временный словарь медиану
        values_size = len(values)

        # Добавляем в временный словарь среднее арифметическое, минимум, максимум
        data['average'] = sum(values)/values_size
        data['min'] = values[0]
        data['max'] = values[-1]

        if values_size % 2 == 0:
            data['median'] = (values[values_size//2-1]+values[values_size//2])/2
        else:
            data['median'] = values[values_size//2]

        # Добавляем значения в главный словарь
        result[interval_values[0][0]] = data

        # Увеличиваем значение индекса, по которому делается срез
        if index + 100 > times_length:
            index = times_length
        else:
            index += 100

    return result


def get_grouped_day_dict(data: dict) -> dict:
    """ Данный метод группирует данные по дням
    Возвращает словрь, содержащий дату, время и данные
    Пример: {'2016-11-10': [{'11:15:21': {...}}, ...], ...}

    :param data: данные в виде словаря, которые нужно группировать по дням
    """

    result = {}

    # Группируем данные по дням
    for key, value in data.items():
        if not key[:10] in result:
            result[key[:10]] = []

        # Группировка типа {'2016-11-03': [{'11:13:30': ...}]}
        result[key[:10]].append({f"{key[11:]}": value})

        # Группировка типа {'2016-11-03': [{'2016-11-03 11:13:30': ...}]}
        # red[key[:10]].append({f"{key}": value})
    return result


if __name__ == '__main__':
    # Берем начальную дату и время и приводит к типу timestamp
    start_time = int(datetime.timestamp(datetime.strptime('2016-11-03 00:00:01', '%Y-%m-%d %H:%M:%S')))

    # Создаем словарь с датой, временем и рандомным значением
    time_dict = get_new_datetime_dict(start_time, 86404)

    # Получаем данные с 100 секундным интервалом
    interval_data = get_100second_interval_dict(time_dict)

    # Группируем данные по дням с интервалом 100 секунд
    grouped_data = get_grouped_day_dict(interval_data)

    # print(grouped_data)
