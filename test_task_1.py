import pytest

from task_1 import get_unique_element_list


@pytest.mark.parametrize('values, k, result', [
    ([1, 2, 5, 3, 4, 4, 5, 6, 7, 8, 9, 10, 11], 4, [1, 2, 3, 5]),
    ([1, 2, 2, 5, 4, 4, 7, 6, 7, 8, 8, 1, 2], 13, [1, 2, 4, 5, 6, 7, 8]),
    ([1, 2, 1, 2, 1, 5, 2, 1, 2, 5, 1, 1, 9], 12, [1, 2, 5]),
])
def test_task_1(values, k, result):
    assert get_unique_element_list(values, k) == result