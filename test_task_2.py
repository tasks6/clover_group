from datetime import datetime

import pytest

from task_2 import get_100second_interval_dict, get_grouped_day_dict


@pytest.mark.parametrize('values, result', [
    (
        {
            '2016-11-03 00:00:01': 17
        },
        {'2016-11-03 00:00:01': {'average': 17.0, 'min': 17, 'max': 17, 'median': 17}},
    ),
    (
        {
            '2016-11-03 00:00:01': 85,
            '2016-11-03 00:00:02': 57
        },
        {'2016-11-03 00:00:01': {'average': 71.0, 'min': 57, 'max': 85, 'median': 71.0}}
    ),
    (
        {
            '2016-11-03 00:00:01': 56, '2016-11-03 00:00:02': 2, '2016-11-03 00:00:03': 10,
            '2016-11-03 00:00:04': 95, '2016-11-03 00:00:05': 11, '2016-11-03 00:00:06': 32,
            '2016-11-03 00:00:07': 7, '2016-11-03 00:00:08': 84, '2016-11-03 00:00:09': 83,
            '2016-11-03 00:00:10': 89
        },
        {'2016-11-03 00:00:01': {'average': 46.9, 'min': 2, 'max': 95, 'median': 44.0}}
    ),
])
def test_small_data_interval_dict(values, result):
    assert get_100second_interval_dict(values) == result


def test_big_data_interval_dict():
    start_time = int(datetime.timestamp(datetime.strptime('2016-11-03 00:00:01', '%Y-%m-%d %H:%M:%S')))
    datetime_dict = {}
    for second in range(301):
        datetime_dict[f'{datetime.fromtimestamp(start_time + second)}'] = second

    result = {
        '2016-11-03 00:00:01': {'average': 49.5, 'min': 0, 'max': 99, 'median': 49.5},
        '2016-11-03 00:01:41': {'average': 149.5, 'min': 100, 'max': 199, 'median': 149.5},
        '2016-11-03 00:03:21': {'average': 249.5, 'min': 200, 'max': 299, 'median': 249.5},
        '2016-11-03 00:05:01': {'average': 300.0, 'min': 300, 'max': 300, 'median': 300}
    }

    assert get_100second_interval_dict(datetime_dict) == result


def test_day_group_data():
    data = {
        '2016-11-03 00:00:01': {'average': 49.5, 'min': 0, 'max': 99, 'median': 49.5},
        '2016-11-03 00:01:41': {'average': 149.5, 'min': 100, 'max': 199, 'median': 149.5},
        '2016-11-04 00:03:21': {'average': 249.5, 'min': 200, 'max': 299, 'median': 249.5},
        '2016-11-04 00:05:01': {'average': 300.0, 'min': 300, 'max': 300, 'median': 300},
        '2016-11-05 00:05:01': {'average': 300.0, 'min': 300, 'max': 300, 'median': 300},
        '2016-11-06 00:05:01': {'average': 200.0, 'min': 100, 'max': 300, 'median': 200},
    }

    result = {
        '2016-11-03': [
            {'00:00:01': {'average': 49.5, 'min': 0, 'max': 99, 'median': 49.5}},
            {'00:01:41': {'average': 149.5, 'min': 100, 'max': 199, 'median': 149.5}}
        ],
        '2016-11-04': [
            {'00:03:21': {'average': 249.5, 'min': 200, 'max': 299, 'median': 249.5}},
            {'00:05:01': {'average': 300.0, 'min': 300, 'max': 300, 'median': 300}}
        ],
        '2016-11-05': [
            {'00:05:01': {'average': 300.0, 'min': 300, 'max': 300, 'median': 300}}
        ],
        '2016-11-06': [
            {'00:05:01': {'average': 200.0, 'min': 100, 'max': 300, 'median': 200}}
        ],
    }

    assert get_grouped_day_dict(data) == result